/**
 * 
 * Higher Order Component used to wrap components for better responsiveness.
 */

const AppContainer = ({ children }) => {
  return <div className="container-fluid p-0">{children}</div>
}

export default AppContainer
