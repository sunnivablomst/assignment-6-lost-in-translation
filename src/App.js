import './App.css'
import { BrowserRouter,
Switch,
Route, } from 'react-router-dom'
import Login from './components/Login/Login'
import Translations from './components/TranslateFeed/Translations'
import NotFound from './components/NotFound/NotFound'

import Profile from './components/Profile/Profile'

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Switch>
          <Route path="/" exact component={Login} />
          <Route path="/translations" component={Translations} />
          <Route Path="/profile" component = {Profile}/>
          <Route path="*" component={NotFound} />
        </Switch>
      </BrowserRouter>
      </div>  
  )
}

export default App
