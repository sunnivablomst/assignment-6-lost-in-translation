import { ACTION_SESSION_INIT, ACTION_SESSION_SET } from "../actions/sessionActions"
import { ACTION_TRANSLATE_DELETE } from "../actions/translateActions"

const initialState = {
  username: "",
  id: "",
  translations: [],
  loggedIn: false,
}

export const sessionReducer = (state = initialState, action) => {
  switch (action.type) {
    case ACTION_SESSION_SET:
      return {
        ...action.payload,
        loggedIn: true,
      }
    case ACTION_TRANSLATE_DELETE:
      return {
        ...initialState,
        loggedIn:true
      }
    case ACTION_SESSION_INIT:
      return{
        ...state
      }
    default:
      return state
  }
}
