import {
  ACTION_SESSION_DELETE,
  ACTION_SESSION_INIT,
  ACTION_SESSION_SET,
  sessionSetAction,
} from "../actions/sessionActions"

export const sessionMiddleware =
  ({ dispatch }) =>
  (next) =>
  (action) => {
    next(action)


    /**
     * Checks the localstorage for an existing session
     * if there is, the session is set to current session
     */

    if (action.type === ACTION_SESSION_INIT) {
      const storedSession = localStorage.getItem("lit-ss")
      if (!storedSession) {
        return
      }
      const session = JSON.parse(storedSession)
      dispatch(sessionSetAction(session))
    }

    /**
     * Sets the session
     */
    if (action.type === ACTION_SESSION_SET) {
      localStorage.setItem("lit-ss", JSON.stringify(action.payload))
    }

    /**
     * Clears the session from local storage.
     */
    if (action.type === ACTION_SESSION_DELETE) {
      localStorage.clear()
    }
  }

