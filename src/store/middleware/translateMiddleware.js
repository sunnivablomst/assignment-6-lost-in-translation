import { LoginAPI } from "../../components/Login/loginAPI"
import { sessionSetAction } from "../actions/sessionActions"
import {
  ACTION_TRANSLATE_SET,
  ACTION_TRANSLATE_DELETE,
  translateErrorAction,
  translateSuccessAction,
} from "../actions/translateActions"

export const translateMiddleware =
  ({ dispatch }) =>
  (next) =>
  async (action) => {
    next(action)
    
    /**
     * Gets session from local storage
     * Updates the translations in the database for the current user based on the values
     * from the session.translations array and adds the new translation.
     * then the user is fetched again from the databse with the updated values and sets a new session with that user.
     * Dispatches translateErrorAction if an error arises
     */
    if (action.type === ACTION_TRANSLATE_SET) {
      try {
        const storedSession = localStorage.getItem("lit-ss")
        const session = JSON.parse(storedSession)
        await LoginAPI.updateTranslations(session.id, [...session.translations, action.payload,])
        const user = await LoginAPI.login({ username: session.username })
        dispatch(sessionSetAction(user[0]))
        dispatch(translateSuccessAction(action.payload))
      } catch (e) {
        dispatch(translateErrorAction(action.payload))
      }
    }


    /**
     * Gets current session from the local storage
     * Clears the users translations by initializing an empty array
     * Fetces the user from the database and updates the values in translations with the empty array.
     * 
     * dispatches translateErrorAction if an error arises
     */
    
    if (action.type === ACTION_TRANSLATE_DELETE) {
      try {
        const storedSession = localStorage.getItem("lit-ss")
        const session = JSON.parse(storedSession)
        await LoginAPI.updateTranslations(session.id, [])

        const user = await LoginAPI.login({ username: session.username })
        dispatch(sessionSetAction(user[0]))
        dispatch(translateSuccessAction(action.payload))
      } catch (e) {
        dispatch(translateErrorAction(action.payload))
      }
    }
  }
