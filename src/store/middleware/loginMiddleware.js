import { LoginAPI } from "../../components/Login/loginAPI"
import {
  ACTION_LOGIN_ATTEMPTING,
  loginErrorAction,
  loginSuccessAction,
} from "../actions/loginActions"

/**
 * If the action.type is ACTION_LOGIN_ATTEMPTING
 *  
 * Throws and error if the payload is empty
 * 
 * If not, it tries to log in to the API and fetch the user.
 * if the user doesnt exist, the user is registered and logged in
 * Dispatches newUser[0] or user[0] on loginSuccessAction based on if it's an exisiting user or not.
 */

export const loginMiddleware =
  ({ dispatch }) =>
  (next) =>
  async (action) => {
    next(action)

    if (action.type === ACTION_LOGIN_ATTEMPTING) {
      try {
        if (action.payload === "") {
          throw new Error("Can't be empty string")
        }
        const user = await LoginAPI.login({ username: action.payload })
        if (user.length === 0) {
          await LoginAPI.register({ username: action.payload })
          const newUser = await LoginAPI.login({ username: action.payload })
          return dispatch(loginSuccessAction(newUser[0]))
        }
        return dispatch(loginSuccessAction(user[0]))
      } catch (e) {
        dispatch(loginErrorAction(e.message))
      }
    }
  }

