
import { useSelector } from "react-redux"
import styles from './TranslateFeed.module.css'

const Translation = () => {
      //gathers state values from store.
    const  {translation} = useSelector((state) => state.translateReducer)
    //regular expression to detect if the user input is consisting of only uppercase and lowercase letters.
    const regex = /[A-Za-z]/
    

    //Displays indivual signs based on character input, images are retrieved from the public folder.
    return(
        <div className={styles.sectionOutputField}>
            <section className={styles.outputCard}>
            <ul>
            {[...translation].map((char, index) => {
                if (!regex.test(char)) {
                return <span></span>
                }
                return (
                <img
                    key={index}
                    src={`/individial_signs/${char}.png`}
                    alt={char}
                />
                )
            })}
            </ul>
            </section>
        </div>
 
    )

// styles.outputfield styles.outputcard fra translatefeed module

}
export default Translation




