
import { useState } from "react"
import { useDispatch, useSelector } from "react-redux"
import {  Redirect } from "react-router-dom"
import AppContainer from "../../hoc/AppContainer"
import { translateClearAction, translateSetAction } from "../../store/actions/translateActions"
import Input from "../Input/Input"
import styles from './TranslateFeed.module.css'
import Translation from "./Translation"
import { useHistory } from "react-router"

const Translations = () => {

  // Implements useHistory, useDispatch, and gathers state values from store.
  const [translation, setTranslation] = useState("")
  const { username } = useSelector((state) => state.sessionReducer)
  const { loggedIn } = useSelector((state) => state.sessionReducer)
  const dispatch = useDispatch()
  const history = useHistory()
  
  const onInputChange = (e) => {
    setTranslation(e.target.value)
  }
  
  const onButtonClick = () => {
    dispatch(translateSetAction(translation))
  }
  
  //onclick effect to redirect the user to the profile page.
  const redirect = () => {
    dispatch(translateClearAction())
    history.push("/profile")
  }
  
  
  return (
  <>
   { !loggedIn && <Redirect to="/" /> }
      <AppContainer>
        <main className={styles.translateHome}>
        <section className={styles.navBar}>
          <h1 className={styles.headerTitle}>Lost in Translation</h1>                           
          <img className={styles.logo} alt="banner logo" src="/LostInTranslation_Resources/Logo-Hello.png"/>
          
          <div className={styles.profile}>
           <button onClick={redirect} className={styles.username}>{username}</button>
          </div>               
        </section>
        <section className={styles.sectionInputField}>
          <Input
          onButtonClick={onButtonClick}
          onInputChange={onInputChange}
          >   
          </Input>
        </section>
          <section className={styles.SectionOutputField}>
            <Translation>
            </Translation>
          </section>
        </main>
      </AppContainer>
  </>
  )
}
export default Translations
