import { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useHistory } from 'react-router-dom'
import AppContainer from '../../hoc/AppContainer'
import { loginAttemptAction, loginDeleteAction } from '../../store/actions/loginActions'
import { sessionSetAction } from '../../store/actions/sessionActions'
import styles from './Login.module.css'
const Login = () => {

  // Creates a local state for changing input
  const [input, setInput] = useState("")

  // Implements useHistory, useDispatch, and gathers state values from store.
  const { username } = useSelector((state) => state.sessionReducer)
  const history = useHistory()
  const dispatch = useDispatch()
  const { loginError, loginAttempting, user } = useSelector((state) => state.loginReducer)

  
  const onInputChange = (e) => {
    setInput(e.target.value)
  }

  const onButtonClick = (event) => {
    event.preventDefault()
    dispatch(loginAttemptAction(input))
  }

  /**
   * Redirects the user to the translationspage if username is not empty.
   * if user !== "" , reset user to inital state, dispatches methods from store to initiate session 
   */
  useEffect(() => {
    if (username !== "") {
      history.push("/translations")
    }
    if (user !== "") {
      dispatch(sessionSetAction(user))
      dispatch(loginDeleteAction())
      history.push("/translations")
    }
  }, [username, user, dispatch, history])
    
    return (
        <>
            <AppContainer>
                <main className={ styles.home }>
                  <div className={styles.headerTitle}>
                    <h1>Lost in Translation</h1>
                  </div>
                    <section className={styles.banner}>
                    
                        <div className={styles.bannerLogo}>
                        <img className = {styles.imgSplash} src= "/LostInTranslation_Resources/Splash.svg" alt="nice"/>      
                        <img  className={styles.logo} alt="banner logo" src={process.env.PUBLIC_URL + '/LostInTranslation_Resources/Logo.png'} />
                        </div>
                        <div className={styles.bannerText}>
                            <h1>Lost in Translation</h1>
                            <h3>Get started</h3>
                        </div>
                    </section>  
                <section className={ styles.inputHome }>
                    <form onSubmit={ onButtonClick }>             
                        <input className={styles.loginInput} id="input" 
                            type="text" 
                            placeholder="What's your name?" 
                           
                            onChange={ onInputChange } />
                    <button type="submit" className={styles.loginButton}>Login</button>
                </form>
                </section>  
            { loginAttempting &&
            <p>Trying to login...</p>
            }
            { loginError && 
                    <div className="alert alert-danger" role="alert">
                        <h4>Unsuccessful</h4>
                        <p className="mb-0">{ loginError }</p>
                    </div>
            }
            </main>
            </AppContainer>      
        </>    
    )
}
export default Login