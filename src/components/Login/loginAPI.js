const API_KEY = 'OKiHkIa16vPsx7vQcoEior9HxfBo7zMiWBMBOl0Jkm5FaRulc0F6MtnAjmPuYHBR'
const API_URL = 'https://assignment5-trivia-game.herokuapp.com'


export const LoginAPI = {

    /**
     * 
     * @param {*} details object consisting of a username
     * @returns the user if user.ok is true , throws an error if not.
     */
    async login(details) {
      try {
        const user = await fetch(
          `${API_URL}/translations?username=${details.username}`
        )
  
        if (!user.ok) {
          throw new Error("Could not retrieve user")
        }
        return await user.json()
      } catch (error) {
        console.error(error)
      }
    },

    /**
     * 
     * @param {*} details object consisting of a username
     * @returns response code 201 creat if user.ok is true, throws an error if not
     */
    async register(details) {
      const requestOptions = {
        method: "POST",
        headers: {
          "X-API-Key": API_KEY,
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          username: details.username,
          translations: [],
        }),
      }
  
      try {
        const user = await fetch(`${API_URL}/translations`, requestOptions)
  
        if (!user.ok) {
          throw new Error("Could not create new user.")
        }
  
        return await user.json()
      } catch (error) {
        console.error(error)
      }
    },
  
    /**
     * updates the database with translations
     * @param {*} userId id of the user
     * @param {*} translations an array of translations that will be stored in the database
     */
    async updateTranslations(userId, translations) {
      const requestOptions = {
        method: "PATCH",
        headers: {
          "X-API-Key": API_KEY,
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          translations: translations,
        }),
      }
      try {
        const response = await fetch(
          `${API_URL}/translations/${userId}`,
          requestOptions
        )
        if (!response.ok) {
          throw new Error("Could not update translation")
        }
      } catch (error) {
        console.error(error)
      }
    },
  }
  