import styles from './Input.module.css'


/**
 * Input field Component used in the Translations Component
 */
const Input = (props) => {
    return(
       
        <div className= {styles.form}>
            <input
            id="input" 
            className={styles.InputTranslate}
            onChange={props.onInputChange}
            placeholder="Write Something To Translate"
            >
            </input>
            <button className={styles.translateButton} onClick={props.onButtonClick}>Go</button>  
        </div>
    )
}

export default Input;