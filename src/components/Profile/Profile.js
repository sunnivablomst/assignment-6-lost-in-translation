import AppContainer from "../../hoc/AppContainer"
import { useDispatch, useSelector } from "react-redux"
import {  Redirect } from "react-router-dom"
import { useHistory } from "react-router"
import { sessionDeleteAction } from "../../store/actions/sessionActions"
import { translateClearAction, translateDeleteAction } from "../../store/actions/translateActions"
import styles from './Profile.module.css'

const Profile = () => {
    
      // Implements useHistory, useDispatch, and gathers state values from store.
    const { username, translations } = useSelector(
        (state) => state.sessionReducer
    )
    const { loggedIn } = useSelector((state) => state.sessionReducer)
    const dispatch = useDispatch()
    const history = useHistory()

    const onClearClick = () => {
        dispatch(translateDeleteAction())
    }

    const onReturnClick = () => {
        history.push("/translations")
    }

    const onLogoutClick = () => {
        dispatch(sessionDeleteAction())
        window.location.reload(false)
    }

      //onclick effect to redirect the user to the profile page.
  const redirect = () => {
    dispatch(translateClearAction())
    history.push("/profile")
  }

    return (
        <>
           { !loggedIn && <Redirect to="/" /> }
            <AppContainer>
            <main>
                <main className={ styles.homeProfile }>
                <section className={styles.navBar}>
                    <h1 className={styles.headerTitle}>Lost in Translation</h1>                           
                    <img className={styles.logo} alt="banner logo" src={process.env.PUBLIC_URL + '/LostInTranslation_Resources/Logo-Hello.png'}/>
                    <div className={styles.profile}>
                     <button onClick={redirect} className={styles.username}>{username}</button>
                    </div>               
                </section>

                    <div className ={ styles.inner }>
                        <div className={ styles.innerProfile }>
                            <h1> {username}'s profile</h1>
                            <h4 className={styles.lastTranslations}>Your last translations</h4>
                            <ul className={styles.translations}>
                            {translations
                            .map((translation, index) => {
                                return (
                                    <li key={index} className={styles.translationItem}>
                                    {translation}
                                    </li>
                                )
                            })
                            .slice(Math.max(translations.length - 10, 0))}
                            </ul>
                        </div>
                        <div>
                            <div className = {styles.profileButtons}>
                                <button onClick={ onReturnClick } className={styles.profileButton} >
                                 New translation
                                </button>
                                <button onClick={ onClearClick } className={styles.profileButton}>
                                 Delete translations
                                </button>
                                <button onClick={ onLogoutClick } className={ styles.logoutButton }>Log out</button>                                
                            </div>                          
                        </div>                    
                    </div>                  
                </main>            
            </main>
            </AppContainer>
        </>       
    )
}
export default Profile