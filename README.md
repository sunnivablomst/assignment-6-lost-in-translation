# Lost in Translation

This project was made by using React framework. 

The application can be launched from: https://safe-mesa-57388.herokuapp.com/

## Descritption
The application translates written language to American sign language (ASL), and consists of:
 - A homepage
 - A translation page
 - A profile page
 - API

## Functionality

### Homepage
The homepage takes input from a user and logs them in if the user exists in the database. If the user doesn't exist, it will register a new user to the database and then log them in. The logged in user is stored in the browser session. When the login is successful, the user is sent to the translation page. If the register/login is unsuccessful, an error message is displayed.

### Translation page
The translation page takes input from a user (max 42 characters), and displays the signs for each character.

### Profile page
The profile page shows the logged in user and their 10 last translations.
From the profile page a user can delete their translations from the session and API, go back to the translation page, and log out through buttons. 

## API
#### Translations API
Api used to store user information and used to verify or register users when logging in.

## Contributors
    * Ludvig Ånestad
    * Sunniva Stolt-Nielsen
